# Space Oddities #

App realizada como prueba práctica para la empresa Space Dev

[Space Oddities](https://space-oddities-app.firebaseapp.com/)

## Instalación

Desde la carpeta spacedev-app, hacemos:

```
npm install
```
* Para correr el sitio web:
```
npm run web-start
```
* Para instalar la app en un celular android:
```
react-native run-android
```

>Nota: No pude probar en ios por falta de apple developer account. Android fue probado en mi celular Moto G3 con Android 6.0

## Estructuras de  la aplicación

### Estructura de componentes

```
Index
. 
└─── Root
      └─── Layout
            └── Main
               	 ├── Footer
                 │      
                 │
                 └── VisibleEventsList
                       └── EventsList
                              ├─- Artist
            				  └── Event
            

```

### State json (resumido)
```
{
  artist: {
    id: '510',
    name: 'Maroon 5',
    url: 'https://www.bandsintown.com/a/510?came_from=267&app_id=test',
    image_url: 'https://s3.amazonaws.com/bit-photos/large/8194061.jpeg',
    thumb_url: 'https://s3.amazonaws.com/bit-photos/thumb/8194061.jpeg',
    facebook_page_url: 'https://www.facebook.com/maroon5',
    mbid: '0ab49580-c84f-44d4-875f-d83760ea2cfe',
    tracker_count: 4133891,
    upcoming_event_count: 37
  },
  artist_events: [
    {
      id: '19879710',
      artist_id: '510',
      url: 'https://www.bandsintown.com/e/19879710?app_id=test&came_from=267',
      on_sale_datetime: '',
      datetime: '2017-12-30T21:00:11',
      venue: {
        name: 'Mandalay Bay Events Center',
        latitude: '36.175',
        longitude: '-115.1363889',
        city: 'Las Vegas',
        region: 'NV',
        country: 'United States'
      },
      offers: [],
      lineup: [
        'Maroon 5'
      ]
    },
    .
    .
    .
    ]}
```
### Actions json 

```
[{
  type: 'RECEIVE_EVENTS',
  response
 },
 {
  type: 'RECEIVE_ARTIST',
  response
 }
 ]
```