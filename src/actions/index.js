import * as api from '../bandsintown_api';
import {RECEIVE_ARTIST, RECEIVE_EVENTS} from '../constants/ActionTypes'

export const searchEvents = (artistName) => (dispatch) =>

  api.searchEvents(artistName).then(events => {
  	events = events || [];

    dispatch({
      type: RECEIVE_EVENTS,
      response: events
    });
  });

  export const searchArtist = (artistName) => (dispatch) =>

  api.searchArtist(artistName).then(artist => {
  	artist = artist || {};

    dispatch({
      type: RECEIVE_ARTIST,
      response: artist
    });
  });