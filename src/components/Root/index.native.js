import React, {Component} from 'react';
import { Provider } from 'react-redux';
import Layout from '../Layout';
import Main from '../Main';

const Root = ({store}) => (
  <Provider store={store}>

      <Layout/>

  </Provider>
);


export default Root;

