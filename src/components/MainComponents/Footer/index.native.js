import React, {Component} from 'react';
import {
  View, Text, StyleSheet
} from 'react-native'

const Footer = () => (
  <View>
    <Text style={styles.muted}>Author: Diego Zuluaga</Text>
  </View>
);

export default Footer;

const styles = StyleSheet.create({
   muted: {
    padding: 8,
    textAlign: 'center'
  }
})