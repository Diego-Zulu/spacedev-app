import React from 'react';
import styled from 'styled-components';

const Footer = () => (
  <BottomDiv>
    <small className="text-muted">Author: Diego Zuluaga</small>
  </BottomDiv>
);

export default Footer;

const BottomDiv = styled.div`
  bottom: 0;
  padding: 1rem;
`;