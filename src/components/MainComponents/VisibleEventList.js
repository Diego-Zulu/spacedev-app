import React, { Component } from 'react';
import { connect } from 'react-redux';
import EventList from './EventList';
import { getEvents, getArtist } from '../../reducers';

class VisibleEventList extends Component {
  
  render() {
    return <EventList {...this.props}/>
  }
}

const mapStateToProps = (state, ownProps) => {
  return {
    events: getEvents(
      state
    ),
    artist: getArtist(state)
  };
};

VisibleEventList = connect(
  mapStateToProps
)(VisibleEventList);

export default VisibleEventList;