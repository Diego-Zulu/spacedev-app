import React, { Component } from 'react';
import styled from 'styled-components';
import {Table} from 'react-bootstrap';

class Artist extends Component {

  render() {

  const {name, url, image_url, facebook_page_url} = this.props;

  return (
  <Table responsive>
  <tr>
      <td className="col-md-6"><ImageWithWidth className="img-responsive pull-right" src={image_url} alt={name + " profile photo"} /> </td>
      <td className="col-md-6 text-left align-middle">
	      	<h3>{name}</h3>
	      	<a href={facebook_page_url} target="_blank" rel="noopener noreferrer">Facebook</a> <br />
	      	<a href={url} target="_blank" rel="noopener noreferrer">BandsInTown</a>
        </td>
    </tr>

      	

    </Table>
)
  }
}

export default Artist;

const ImageWithWidth = styled.img`
   width: 50%
`;