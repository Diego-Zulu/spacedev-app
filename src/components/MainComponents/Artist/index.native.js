import React, { Component } from 'react';
import {
  View,
  Image,
  Text,
  Button,
    StyleSheet, Linking
} from 'react-native'

class Artist extends Component {

  render() {
  const {name, url, image_url, facebook_page_url} = this.props;

  return (
    <View style={styles.view}>
    <Image
          style= {styles.image}
          source={{uri: image_url}}
        />
    <Text style={styles.title}>{name}</Text>
     <View style={styles.view}>
        
        <Button color="#4CAF50" style={styles.bandsInTown_btn} title="BandsInTown" onPress={ ()=>{ Linking.openURL(url)}} />
        <Button color="#008CBA" style={styles.facebook_btn} title="Facebook" onPress={ ()=>{ Linking.openURL(facebook_page_url)}} />
      </View>
    </View>
     
)
  }
}

export default Artist;

const styles = StyleSheet.create({
   title: {
    fontWeight: 'bold',
    textAlign: 'center',
    fontSize: 20,
    padding: 8,
    marginLeft: 16,
    marginRight: 16,
  },
  view: {
    padding: 8,
    justifyContent: 'center',
    alignItems: 'center',
  },
  text: {
    flex: 1
  },
  bandsInTown_btn: {
    flex: 1
  },
  facebook_btn: {
    flex: 1
  },
  image: {
    borderRadius: 0.5,
    padding: 8,
    height:300, width: 300,
    marginBottom: 8,
  },
})