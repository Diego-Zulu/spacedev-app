import React, { Component } from 'react';
import Event from '../Event';
import Artist from '../Artist';
import {
  ListView, ScrollView, Text, StyleSheet
} from 'react-native'


class EventList extends Component {

  render() {
    const ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});
    return (
      <ScrollView>
      <Artist {...this.props.artist}/>
      <Text style={styles.title}> Events: </Text>
      <ListView
        dataSource={ds.cloneWithRows(this.props.events)}
        renderRow={event =>
      <Event 
        key={event.name}
        {...event}
        
      />}
      />
       {this.props.events.length === 0 && this.props.artist.name !== undefined && <Text style={styles.message}> We don't know about any events from this artist! </Text>}
      </ScrollView>
    );
  }
}

export default EventList;

const styles = StyleSheet.create({
   title: {
    fontWeight: 'bold',
    textAlign: 'center',
    fontSize: 14,
    padding: 8,
    marginLeft: 16,
    marginRight: 16,
  },message: {
    textAlign: 'center',
  }
})
