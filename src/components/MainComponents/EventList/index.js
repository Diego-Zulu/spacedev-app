import React from 'react';
import Event from '../Event';
import Artist from '../Artist';
import { Table} from 'react-bootstrap';

const EventList = ({events, artist}) => (
  <div>
  {artist.name !== undefined && <Artist {...artist}/>}
  {events.length > 0 && 
    <Table responsive>
      <thead>
      <tr>
        <th className="text-center">Date</th>
        <th className="text-center">Venue</th>
        <th className="text-center">Location</th>
        <th className="text-center"></th>
      </tr>
    </thead>
    <tbody>
    {events.map(event =>
      <Event
        key={event.id}
        {...event}
      />
    )}
    </tbody>
  </Table>
  }
    {events.length === 0 && artist.name !== undefined && <h3 className="text-muted"> We don't know about any events from this artist! </h3>}
  </div>
);

export default EventList;