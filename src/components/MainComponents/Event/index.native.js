import React, { Component } from 'react';
import {
  View,
  Text,
  Button,
    StyleSheet, Linking
} from 'react-native'

class Event extends Component {

  render() {
  const {datetime, url, venue} = this.props;
  let date = new Date(datetime);
  date = date.toLocaleString();

  return (
      <View style={styles.view}>
    <Text style={styles.text}>{date}</Text>
    <Text style={styles.text}>{venue.name}</Text>
    <Text style={styles.text}>{venue.city}, {venue.country}</Text>
    <Button color="#4CAF50" style={styles.button} title="BandsInTown" onPress={ ()=>{ Linking.openURL(url)}} />
  </View>
)
  }
}

export default Event;

const styles = StyleSheet.create({
  view: {
    padding: 8,
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'row'
  },
  text: {
    flex: 1
  },
  button: {
    flex: 1
  }
})