import React, { Component } from 'react';

class Event extends Component {

  render() {
  const {datetime, url, venue} = this.props;
  let date = new Date(datetime);
  date = date.toLocaleString();

  return (

  <tr>
  	<td>{date}</td>
  	<td>{venue.name}</td>
  	<td>{venue.city}, {venue.country}</td>
  	<td><a href={url} target="_blank" rel="noopener noreferrer">BandsInTown</a></td>
  </tr>
)
  }
}

export default Event;