import React, {Component} from 'react';
import VisibleEventList from '../MainComponents/VisibleEventList';
import Main from '../Main';
import {StackNavigator } from 'react-navigation';

const Layout = StackNavigator({
  Main: {screen: Main,

  navigationOptions: ({navigation}) => ({
      title: 'Home',
      headerTitleStyle: {
      color: '#9d9d9d',
   },
   headerStyle: {
   	  backgroundColor: '#222222'
   },
    })},
  Events: {screen: VisibleEventList,

  navigationOptions: ({navigation}) => ({
      title: `${navigation.state.params.artist_name}`,
      headerTitleStyle: {
      color: '#9d9d9d',
   },
   headerStyle: {
   	  backgroundColor: '#222222'
   },
    })}
});


export default () => <Layout />;

