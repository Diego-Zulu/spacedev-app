import React, { Component } from 'react';
import Main from '../Main';
import { Route } from 'react-router-dom';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { Navbar, Button, FormControl, FormGroup, Form} from 'react-bootstrap';
import { searchEvents, searchArtist } from '../../actions/index';

class Layout extends Component {

  getInfoFromApiOnSubmit(form, inputText) {
          form.preventDefault();
          if (!inputText) {
            return;
          }
         this.props.searchEventsClick(inputText);
         this.props.searchArtistClick(inputText);
        }

render() {
  let input;

  return (
  <div>
  <Navbar inverse collapseOnSelect fluid staticTop>
    <Navbar.Header>
      <Navbar.Brand>
        <Link to={"/"}>Space Oddities</Link>
      </Navbar.Brand>
      <Navbar.Toggle />
    </Navbar.Header>
    <Navbar.Collapse>
      <Navbar.Form  pullRight>
      <Form onSubmit={form => this.getInfoFromApiOnSubmit(form, input.value.trim())}>
        <FormGroup>
          <FormControl inputRef={node => { input = node;}} type="text" placeholder="Search Artists" />
        </FormGroup>
        {' '}
        <Button type="submit">Submit</Button>
        </Form>
      </Navbar.Form>
    </Navbar.Collapse>
  </Navbar>
      <div className="text-center center-block"> 
        <Route exact path='/' component={Main}></Route>
      </div>
  </div>
)}
}

const mapDispatchToProps = (dispatch) => {
  return {
    searchEventsClick: (artist) => {
      dispatch(searchEvents(artist));
    },
    searchArtistClick: (artist) => {
      dispatch(searchArtist(artist));
    }
  };
};

Layout = connect(
  null,
  mapDispatchToProps
)(Layout)

export default Layout;
