import React from 'react';
import Footer from '../MainComponents/Footer';
import VisibleEventList from '../MainComponents/VisibleEventList';

const Main = (props) => {

  
  return (

    <div className="text-center">
      <h1>Space Oddities</h1>
      <VisibleEventList/>
      <Footer />
    </div>

)}

export default Main;