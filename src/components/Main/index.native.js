import React, {Component} from 'react';
import Footer from '../MainComponents/Footer';
import VisibleEventList from '../MainComponents/VisibleEventList';
import { SearchBar } from 'react-native-elements'
import { searchEvents, searchArtist } from '../../actions/index';
import { connect } from 'react-redux';
import {
  View,
  Text,
  Button,
    StyleSheet
} from 'react-native'

class Main extends Component {

  getInfoFromApiOnSearch(inputText) {
    
          if (!inputText) {
            return;
          }
         this.props.searchEventsClick(inputText);
         this.props.searchArtistClick(inputText);
         this.props.navigation.navigate('Events', {artist_name: this.state.search});
        }

        state = {
    search: '',
  }

        handleChangeSearch = search => this.setState({ search })

render() {


  return (
  <View >
      <Text  style= {styles.title}>Space Oddities</Text>
      

  <SearchBar noIcon lightTheme style= {styles.searchBar} value={this.state.search} onChangeText={this.handleChangeSearch}
    onClearText={() => this.getInfoFromApiOnSearch(this.state.search)}
      placeholder='Search Artist' />
      <View style= {styles.center}>
      <Button color="#5bc0de" title="Search" onPress={() => this.getInfoFromApiOnSearch(this.state.search)}/>
      </View>
      <Footer />
    </View>
)}
}

const mapDispatchToProps = (dispatch) => {
  return {
    searchEventsClick: (artist) => {
      dispatch(searchEvents(artist));
    },
    searchArtistClick: (artist) => {
      dispatch(searchArtist(artist));
    }
  };
};

Main = connect(
  null,
  mapDispatchToProps
)(Main)

export default Main;

const styles = StyleSheet.create({
   title: {
    fontWeight: 'bold',
    textAlign: 'center',
    fontSize: 20,
    padding: 8,
    marginLeft: 16,
    marginRight: 16,
  },
  center: {
    padding: 8,
    justifyContent: 'center',
    alignItems: 'center',
  }
})
