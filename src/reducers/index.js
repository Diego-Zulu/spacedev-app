import { combineReducers } from 'redux';
import {RECEIVE_ARTIST, RECEIVE_EVENTS} from '../constants/ActionTypes'

const createList = () => {
  return (state = [], action = {}) => {
    switch (action.type) {
      case RECEIVE_EVENTS:
        return action.response;
      default:
        return state;
    }
  }
};

const createArtist = () => {
  return (state = [], action = {}) => {
    switch (action.type) {
      case RECEIVE_ARTIST:
        return action.response;
      default:
        return state;
    }
  }
};

const reducers = combineReducers({
  artist : createArtist(),
  artist_events : createList(), 
});

export default reducers;

export const getEvents = (state) => {
  
  return state.artist_events;
};

export const getArtist = (state) => {
  
  return state.artist;
};

