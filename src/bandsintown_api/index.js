import {app_id, bandsintownBaseUrl} from './apiCredentials'


export const searchEvents = (bandName) =>
	fetch(bandsintownBaseUrl + '/artists/'+ bandName + '/events?' + app_id)
   .then((response) => response.json())
   .then((responseJson) => {
     return responseJson;
   })
   .catch((error) => {
     console.error(error);
   });


export const searchArtist = (bandName) =>
	fetch(bandsintownBaseUrl + '/artists/'+ bandName + '/?' + app_id)
   .then((response) => response.json())
   .then((responseJson) => {
     return responseJson;
   })
   .catch((error) => {
     console.error(error);
   });