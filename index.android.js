/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  AppRegistry,
} from 'react-native';
import configureStore from './src/configureStore';
import Root from './src/components/Root';

const store = configureStore();

export default class SpaceApp extends Component {
  render() {
    return (
      <Root store={store} />
    );
  }
}

AppRegistry.registerComponent('SpaceApp', () => SpaceApp);